ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_CONFIG=/srv/gitlab/config
ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/gitlab" \
      name="GitLab Action Cable Service" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="GitLab Action Cable Service runs the GitLab Rails application with web-socket enabled Puma web server." \
      description="GitLab Action Cable Service runs the GitLab Rails application with websocket-enabled Puma web server."

ADD gitlab-actioncable-ee.tar.gz /
ADD gitlab-python.tar.gz /

COPY scripts/ /scripts
COPY --chown=git configuration/ ${GITLAB_CONFIG}/

ENV GITALY_FEATURE_DEFAULT_ON=1

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf ${DNF_OPTS} install -by --nodocs procps \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chmod 0700 public/uploads \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} public/uploads

USER ${GITLAB_USER}:${GITLAB_USER}

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck

CMD /scripts/process-wrapper
